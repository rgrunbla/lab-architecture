{
  description = "Development environment";
  inputs = { nixpkgs.url = "github:NixOS/nixpkgs/nixos-21.11"; };
  
  outputs = { self, nixpkgs }: with import nixpkgs { system = "x86_64-linux"; overlays = []; }; {

    packages.x86_64-linux.configuration = callPackage ./pkgs/configuration/configuration.nix { };
    packages.x86_64-linux.client = callPackage ./pkgs/auto_install_iso/auto_install_iso.nix { user-data = "${self.packages.x86_64-linux.configuration.out}/client-user-data.yaml"; };
    packages.x86_64-linux.server = callPackage ./pkgs/auto_install_iso/auto_install_iso.nix { user-data = "${self.packages.x86_64-linux.configuration.out}/server-user-data.yaml"; };

    devShell.x86_64-linux = with import nixpkgs { system = "x86_64-linux"; };
      let
        qemuNetworkInternalServer = ''-device virtio-net,netdev=int0 -netdev tap,id=int0,ifname=tap0,script=no,downscript=no''; # Lan Server
        qemuNetworkInternalClient = ''-device virtio-net,netdev=int0,mac=0c:c4:7a:73:14:b2 -netdev tap,id=int0,ifname=tap1,script=no,downscript=no''; # Lan Client
        qemuNetworkExternalServer = ''-nic user,hostfwd=tcp::2222-:22''; # Wan Server
        qemuNetworkExternalClient = ''-nic user,hostfwd=tcp::2223-:22''; # Wan Client

        installServer = pkgs.writeShellScriptBin "installServer" ''
          #${pkgs.tunctl}/bin/tunctl -t tap0 -u `whoami`
          ${pkgs.coreutils}/bin/truncate -s 20G server_image.img
          ${pkgs.qemu}/bin/qemu-system-x86_64 -enable-kvm -no-reboot -m 4096 -cdrom ${self.packages.x86_64-linux.server.out}/*.iso -drive file=server_image.img,format=raw,cache=none,if=virtio ${qemuNetworkInternalServer} ${qemuNetworkExternalServer}
        '';
        runServer = pkgs.writeShellScriptBin "runServer" ''
          ${pkgs.qemu}/bin/qemu-system-x86_64 -enable-kvm -no-reboot -m 4096 -drive file=server_image.img,format=raw,cache=none,if=virtio ${qemuNetworkInternalServer} ${qemuNetworkExternalServer}
        '';
        configureServer = pkgs.writeShellScriptBin "configureServer" ''
          ${pkgs.ansible}/bin/ansible-playbook server.yml --extra-vars "@${self.packages.x86_64-linux.configuration.out}/ansible_variables.yaml" -i hosts --ask-become-pass
        '';
        installClient = pkgs.writeShellScriptBin "installClient" ''
          ${pkgs.coreutils}/bin/truncate -s 20G client_image.img
          ${pkgs.qemu}/bin/qemu-system-x86_64 -enable-kvm -no-reboot -m 4096 -cdrom ${self.packages.x86_64-linux.client.out}/*.iso -drive file=client_image.img,format=raw,cache=none,if=virtio ${qemuNetworkInternalClient} ${qemuNetworkExternalClient}
        '';
        runClientInstall = pkgs.writeShellScriptBin "runClientInstall" ''
          ${pkgs.qemu}/bin/qemu-system-x86_64 -enable-kvm -no-reboot -m 4096 -drive file=client_image.img,format=raw,cache=none,if=virtio ${qemuNetworkInternalClient} ${qemuNetworkExternalClient}
        '';
        configureClient = pkgs.writeShellScriptBin "configureClient" ''
          ${pkgs.ansible}/bin/ansible-playbook client.yml --extra-vars "@${self.packages.x86_64-linux.configuration.out}/ansible_variables.yaml" -i hosts --ask-become-pass
        '';
        runClient = pkgs.writeShellScriptBin "runClient" ''
          ${pkgs.qemu}/bin/qemu-system-x86_64 -enable-kvm -no-reboot -m 4096 -drive file=client_image.img,format=raw,cache=none,if=virtio  ${qemuNetworkInternalClient}
        '';
      in
      mkShell {
        buildInputs = [
          dhall
          dhall-json
          haskellPackages.dhall-yaml
          vagrant
          ansible
          installServer
          runServer
          configureServer
          installClient
          runClientInstall
          configureClient
          runClient
        ];
        shellHook = ''
          ansible-galaxy collection install community.docker community.crypto ansible.posix
        '';
      };
  };
}
