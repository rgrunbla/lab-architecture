{ stdenv, coreutils, dhall-json }:

stdenv.mkDerivation {
  name = "configuration";
  dontUnpack = true;
  srcs = [
    ./dhall
  ];

  buildPhase = ''
    srcs=($srcs)
    ${dhall-json}/bin/dhall-to-yaml --file ''${srcs[0]}/client.dhall --output client-user-data.yaml;
    ${dhall-json}/bin/dhall-to-yaml --file ''${srcs[0]}/server.dhall --output server-user-data.yaml;
    ${dhall-json}/bin/dhall-to-yaml --file ''${srcs[0]}/ansible_variables.dhall --output ansible_variables.yaml;
  '';

  installPhase = ''
    ${coreutils}/bin/mkdir $out
    ${coreutils}/bin/echo "#cloud-config" | cat - client-user-data.yaml > $out/client-user-data.yaml
    ${coreutils}/bin/echo "#cloud-config" | cat - server-user-data.yaml > $out/server-user-data.yaml
    ${coreutils}/bin/mv ansible_variables.yaml $out/;
  '';
}
