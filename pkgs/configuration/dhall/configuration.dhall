let utils = ./utils.dhall

let domain = "grunblatt.org"

let ipa_domain = "ipa.${domain}"

let clients_domain = "clients.${domain}"

let ipa_ip_address = "10.0.0.1"

let ipa_cidr = "24"

in  { authorized-keys =
      [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDwZm6kUsK/FrgJPzFi5AgvqIIYfy5S/Se4/rZgB4Edx remy@typhoon"
      ]
    , remote_admin_username = "admin"
    , remote_admin_password = "YOURPASSWORD"
    , ipa_ds_password = "YOURPASSWORD"
    , local_admin_username = "localadmin"
    , local_admin_password =
        "\$y\$j9T\$A4nRpisFwCaMtwMb6jL7q0\$8PiEPOhv7SNso9UDL.JVHLjvp2hLW6/ZeySRe0VhZy5"
    , ipa_domain
    , ipa_realm = utils.upperASCII ipa_domain
    , dhcp_clients_zone = clients_domain
    , dhcp_range_cidr = "10.0.0.0/${ipa_cidr}"
    , dns_server = "8.8.8.8"
    , dhcp_client_range_cidr = "10.0.0.128/25"
    , ipa_ip_address
    , ipa_ip_address_plus_cidr = "${ipa_ip_address}/${ipa_cidr}"
    , delay = 60
    , retries = 3
    , locale = "fr_FR"
    , keyboard = { layout = "fr", variant = "bepo" }
    , client_apt.sources.`vscodium.list`
      =
      { key =
          ''
          -----BEGIN PGP PUBLIC KEY BLOCK-----

          mQINBFu8c8sBEACrPcX4UB0rGFObGzIpa3fqJy0K//W5XENNBrGlk6DS68SwkSza
          QFoD+ZoGiwGZWy+X5m2bm6A0j8ff61uKLZFx4turikggdqey6RL3NIQOmhzbz9jc
          90ZtVG3W2VHwP1Oy2sITJBD1UdCtnRR7ONfoLVJbavPrjHL0VEJqV6kF7ki45DAK
          yVbEGjsg668l8FmfMIYyoqV8HQ+JosXQ6ItOH2QlGwvD5RvBmIRzIXSS159+UHBj
          jrk81Pb1RLWUafoh8Geh4VZHQrvqkLmds/74KZQg47zDr1atKeIRe/p7UPZIEJsr
          puOQP+4yfW8CKN/du62mO37xyJBcQpdsk+27/AAbpluwwzfrRAZcI9wqlmE7CRie
          tARI3eLA/BVeLm0U5Jxemc1asTAptQTij9BvzFfZcpkwK+AcVmaicQhJ9iaa8bQ2
          oTTE5keewHgVdun+XfeMPPuRcF5QxeVK9dZQVI3nSObOXrehulkYXusMQ4vq8bh7
          GktI5n1O2qehnSn/Is7kdLBWDy8xGlgYEWe9oStQ5NSXo4PR7YWDzPs5aXBz9LOa
          zthJqM1Ah41q5/rSgfhM3e+vXtF3M8phLoJTE5mlb370XVH1ZLNbXVvIuHO0L+Lh
          ruZc/tREFWThOiS+FIGv/MDs4DieWEwr9+/598pFERo7cJEo5AZNgPy8SQARAQAB
          tCRQYXZsbyBSdWR5aSA8cGF1bGNhcnJvdHlAcmlzZXVwLm5ldD6JAk4EEwEIADgW
          IQQTAt5gIxiJ/h66ytxUZ4z3WieNnAUCW7xzywIbAwULCQgHAgYVCgkICwIEFgID
          AQIeAQIXgAAKCRBUZ4z3WieNnPDyD/oCSpzl4uua0r/T4cL0rEhHVg82HounlTs2
          BxI0ZhJpjdqIE2ytIFiwdV47yAkt04ow0zEBHbZQldvixfsM3tDYrC6iiC4pRMMc
          W6oVGf18r8nKKRmYTaOSPWL5yhLgO/IkQ/kCBsu3Wf0bp3GbHQ4wny+rmP745oG9
          npGPPW7EiUcFYPIM/YX2YqSH/FOGcMOwlp1QEvVqypQqaogyUmDRP+6bpKYMuq/c
          GuPxHuwPmS7a3Zd6ybAtYBNumC/lNYbfxAZA1NK4WViVGB/P5GBWC0HSfLPsBu7D
          /GqoqYCDTYYtSpnw2kUpkGWqOaWZc9S5Jvp26Hw+VoVGBdJUe7s7qJyeNmdjM/cR
          /dNY9+fW3w9zWgJZcXTnScmpi0vzA7BLmFfsphPBZ7J1Sc+N72uV/W9A70yyNhSw
          DQ20/4D2AHNozmq881LhBlOIw24jb0LlbrA7CFoR10zkpXsS/Vh1EWReV1z4zJDv
          H1SKUzXaOJUpqqW0EpblEpH4qg5hknnnW4XjvlWZO/ICkKTi0LxXK9Lmcbhtzg9t
          Wh7bfDXHXoQYS4QooJbzUhAHwXvQp03R0qu6UhEVhO42y5PVQ/+18FlhgBd+zP1Q
          7Urb93f+7YbSa8e34ANcVvJZc7gP2oRTuVyKVjO/Q9l6+Qzg9DTFVPGYLvYJKIpK
          odUkTbb/ArkCDQRbvHPLARAA3zMxF1XX4tLaz/0U7p381AXmtMA2L63mRQ3YGZxg
          fVxyVx6FdLujxJHytIGnLb9FYQZkxjYyMVc8/7ukrPUTHDUHm2ab4mG0SvhDI3nx
          2qeXE2dYMkpLoBqvFLekFAU129w9BLm9lbHfN+JbbdMmoqlyiufuPM6gz0gBV6ce
          oXUtu8Q2/ixshSfvcdHx21ZD2HuNqSyworbzkA+0B0F51QRp1tqYJ7wQm3n82rQ+
          YFS97Un+7VWgJrX4aofUxRiDx0VHIYkEN8QcDZTQywT4zkj6tDMyKEp40axvZ6zM
          AlaTI1GVrGMUHH7bnZXiF2ZyKHOF6XjTuEfotHXm8fzEHLdhtdUxiQ4+GK5ajQN2
          ay/v5JEtsc3FSAmjfTW9r6jEAiDn5TbeKHtLpeGmtPqvgK/Nzo5rRRx0NgymymeF
          q7Ir4ATu9x8KKYXa/tFd2Qe8rtxNqhYjUh+W7FpMMNXYa18G+PbwNqpEGMdp51rS
          9/Lq2GSYcuCV/jD2QbySUX5wHe9zDNiOuovBFhJZS8H2OGl6aep6zEpV/Q7FcWEG
          9fz4RHADSGB6RFFI8hyV0/YaVU9rB4fAOZzybuJ4DSadOAsVVfkk2WdnUM6x1z24
          XhRdakH+ekGDg+nqs8wUc4u4ouo7dPTPaxbrUas5gdzjbtwoYXoKaIUnisPRranz
          EjsAEQEAAYkCNgQYAQgAIBYhBBMC3mAjGIn+HrrK3FRnjPdaJ42cBQJbvHPLAhsM
          AAoJEFRnjPdaJ42cEq8QAKJ1YUzQsgzUWSzkPgSZJwOAujWkDdhw+YbNvjHAgRZA
          DrNbIyIYkJ/IevubOnteJgwGP/6qaMJwemM+VJ3e/YHvcvAmIilmH33tnnMRqWsj
          xqG021SuX3FGdMXR7WwINZaToh+Lqaj4YXpanjPzGMGKZlzaSdj6avm7KT3HGHjO
          Gk/nz0rAsqXfuzQBkHZ+JozUMqrh30+POcvkyFhqmTQ9juECFzOtegbEWYdDFBMA
          tig1whBccebZ0W+Sva+e1AKktwHNtweaofCg8zQ39Nx1KOz9FoH4B2QF+dYU/yMQ
          BpPYTRQPfbaf2/t5JJaoplWrE2gP+JS8ET11J7U4xtzqs7QprcCurTZv42PjwPKu
          Hc8uwE1VaeX+d7zRGXD9ooR+u+BWKiwxD9p54/QHu/qh/x/4evrETTIZGsheojgm
          srMYT2mW1hLFcOANAgvxuIm+O51+XYviQCGPqg8J2jm5zmxMNwEhVx0ObYcnJqve
          klJniYNw9Ja9gnXxXWycChhmGM/QFLE0yrN7di7oDeN/JUhPPgppCken/xFihoxz
          hlNM8vyJMQiddIBiEcz0zcAbbT74+qFJv9KRsfWAU0MEy3a66H/hybEJu140qTNw
          d7e/dbqns8bDX1BP/x5/QKfxbZNoH2QWmCdv/b5dJ97OXFBtPM/VnOYaaFG5y2gX
          =LCWI
          -----END PGP PUBLIC KEY BLOCK-----
          ''
      , source = "deb https://download.vscodium.com/debs vscodium main"
      }
    , client_bootcmd =
      [ "apt remove -y snapd unattended-upgrades"
      , "mkdir -p /etc/ssl/certs/java/cacerts"
      ]
    , client_packages =
      [ "gnome"
      , "lxde"
      , "ocaml"
      , "jupyter"
      , "inkscape"
      , "gimp"
      , "apache2"
      , "wget"
      , "htop"
      , "language-pack-fr"
      , "vim"
      , "emacs"
      , "pyzo"
      , "zeal"
      , "nodejs"
      , "libreoffice"
      , "mariadb-server"
      , "phpmyadmin"
      , "sqlite3"
      , "git"
      , "gcc-10"
      , "gfortran"
      , "gprolog"
      , "openjdk-8-jdk"
      , "texlive-latex-base"
      , "texlive-lang-french"
      , "texmaker"
      , "qgis"
      , "python3-numpy"
      , "python3-pip"
      , "python3-scipy"
      , "python3-matplotlib"
      , "python3-pandas"
      , "python3-sklearn"
      , "r-base"
      , "graphviz"
      , "gnuplot"
      , "flex"
      , "ml-yacc"
      , "ragel"
      , "menhir"
      , "pandoc"
      , "libreoffice"
      , "libreoffice-l10n-fr"
      , "gnome-terminal"
      , "python-is-python3"
      , "firefox"
      , "chromium-browser"
      , "pyzo"
      , "subversion"
      , "codium"
      , "opam"
      , "zlib1g-dev"
      , "libffi-dev"
      , "libgmp-dev"
      , "libzmq5-dev"
      , "evince"
      ]
    , client_runcmd =
      [ "rm /var/lib/dpkg/info/ca-certificates-java.postinst && apt install -f"
      , "git clone https://gitlab.com/agreg-info/clef-agreg.git"
      , "cd clef-agreg && bash ./scripts/install.sh"
      ]
    }
