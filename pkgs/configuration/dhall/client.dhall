let configuration = ./configuration.dhall

let storage = ./storage.dhall

in  { autoinstall =
      { identity =
        { hostname = "localhost"
        , password = configuration.local_admin_password
        , username = configuration.local_admin_username
        }
      , keyboard = configuration.keyboard
      , locale = configuration.locale
      , network =
        { ethernets = { ens3.dhcp4 = "yes", ens4.dhcp4 = "yes" }, version = 2 }
      , package_update = True
--      , packages = configuration.client_packages
      , ssh =
        { allow-pw = "no"
        , authorized-keys = configuration.authorized-keys
        , install-server = "yes"
        }
      , storage.config
        =
        [ storage.disk
            storage.DiskAction::{
            , path = "/dev/vda"
            , preserve = False
            , grub_device = True
            , id = "disk-1"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Size "1M"
            , number = 1
            , preserve = False
            , id = "partition-grub-1"
            , flag = Some "bios_grub"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Size "1G"
            , number = 2
            , preserve = False
            , id = "partition-boot-1"
            }
        , storage.partition
            storage.PartitionAction::{
            , device = "disk-1"
            , size = storage.SizeType.Fill -1
            , number = 3
            , preserve = False
            , id = "partition-system-1"
            }
        , storage.format
            storage.FormatAction::{
            , fstype = "ext4"
            , volume = "partition-system-1"
            , preserve = False
            , id = "format-system"
            }
        , storage.format
            storage.FormatAction::{
            , fstype = "ext4"
            , volume = "partition-boot-1"
            , preserve = False
            , id = "format-boot"
            }
        , storage.mount
            storage.MountAction::{
            , device = "format-system"
            , path = "/"
            , id = "mount-system"
            }
        , storage.mount
            storage.MountAction::{
            , device = "format-boot"
            , path = "/boot"
            , id = "mount-boot"
            , options = Some "errors=remount-ro"
            }
        ]
      , version = 1
      }
    }
