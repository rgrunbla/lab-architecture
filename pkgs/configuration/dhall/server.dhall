let configuration = ./configuration.dhall

in  { autoinstall =
      { identity =
        { hostname = configuration.ipa_domain
        , password = configuration.local_admin_password
        , username = configuration.local_admin_username
        }
      , keyboard = configuration.keyboard
      , locale = configuration.locale
      , network =
        { ethernets =
          { ens3.dhcp4 = "yes"
          , ens4 =
            { addresses = [ configuration.ipa_ip_address_plus_cidr ]
            , dhcp4 = False
            , dhcp6 = False
            , nameservers.addresses = [ configuration.ipa_ip_address ]
            }
          }
        , version = 2
        }
      , package_update = True
      , packages = [ "console-data" ]
      , ssh =
        { allow-pw = "no"
        , authorized-keys = configuration.authorized-keys
        , install-server = "yes"
        }
      , version = 1
      }
    }
