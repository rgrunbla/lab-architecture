let configuration = ./configuration.dhall

in  { delay = configuration.delay
    , dhcp_client_range_cidr = configuration.dhcp_client_range_cidr
    , dhcp_clients_zone = configuration.dhcp_clients_zone
    , dhcp_range_cidr = configuration.dhcp_range_cidr
    , dns_server = configuration.dns_server
    , ipa_admin_password = configuration.remote_admin_password
    , ipa_admin_username = configuration.remote_admin_username
    , ipa_domain = configuration.ipa_domain
    , ipa_ds_password = configuration.ipa_ds_password
    , ipa_ip_address = configuration.ipa_ip_address
    , ipa_realm = configuration.ipa_realm
    , retries = configuration.retries
    }
