{ stdenv, fetchurl, qemu, cloud-utils, xorriso, findutils, syslinux, gnused, gnutar, coreutils, user-data ? ./user-data }:

stdenv.mkDerivation {
  name = "cagregos";
  dontUnpack = true;
  srcs = [
    (fetchurl {
      url = "https://releases.ubuntu.com/20.04.4/ubuntu-20.04.4-live-server-amd64.iso";
      sha256 = "sha256-KMzbVkUOZDutA7t7z3UHzj2NkOi/CeOPa9msKYqY6q0=";
    })
  ];

  buildPhase = ''
    srcs=($srcs)
    ${xorriso}/bin/xorriso -osirrox on -indev ''${srcs[0]} -extract / ubuntu_iso
    ${coreutils}/bin/chmod -R +w ubuntu_iso
    ${coreutils}/bin/mkdir -p ubuntu_iso/nocloud
    ${coreutils}/bin/touch ubuntu_iso/nocloud/meta-data
    ${coreutils}/bin/cp ${user-data} ubuntu_iso/nocloud/user-data
    ${gnused}/bin/sed -i 's|---|autoinstall ds=nocloud\\\;s=/cdrom/nocloud/ ---|g' ubuntu_iso/boot/grub/grub.cfg
    ${gnused}/bin/sed -i 's|---|autoinstall ds=nocloud;s=/cdrom/nocloud/ ---|g' ubuntu_iso/isolinux/txt.cfg
    ${coreutils}/bin/md5sum ubuntu_iso/.disk/info > ubuntu_iso/md5sum.txt
    ${gnused}/bin/sed -i 's|ubuntu_iso/|./|g' ubuntu_iso/md5sum.txt
    ${coreutils}/bin/mv ubuntu_iso/ubuntu .
    (cd ubuntu_iso; ${findutils}/bin/find '!' -name "md5sum.txt" '!' -path "./isolinux/*" -follow -type f -exec "${coreutils}/bin/md5sum" {} \; > ../md5sum.txt)
    ${coreutils}/bin/mv md5sum.txt ubuntu_iso/
    ${coreutils}/bin/mv ubuntu ubuntu_iso
  '';

  installPhase = ''
    ${coreutils}/bin/mkdir $out
    ${xorriso}/bin/xorriso -as mkisofs -r -o $out/ubuntu-live-amd64-autoinstall.iso -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus -isohybrid-mbr ${syslinux}/share/syslinux/isohdpfx.bin ubuntu_iso/boot ubuntu_iso
  '';
}
