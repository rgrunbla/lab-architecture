---
- hosts: server
  gather_facts: no
  become: true
  tasks:

  - name: Install Docker Dependencies
    apt:
      name:
        - ca-certificates
        - curl
        - gnupg
        - lsb-release
      state: present
      update_cache: yes

  - name: Trust the docker repository GPG key
    apt_key:
      url: https://download.docker.com/linux/ubuntu/gpg
      state: present

  - name: Add  the docker repository to apt
    apt_repository:
      repo: deb https://download.docker.com/linux/ubuntu bionic stable
      state: present

  - name: Install docker
    apt:
      name:
        - docker-ce
        - docker-ce-cli
        - containerd.io
      state: latest
      update_cache: yes

  - name: Install python3-pip
    apt:
      update_cache: yes
      state: latest
      name: python3-pip

  - name: Install python docker interfaces
    pip:
      name:
        - docker
        - docker-compose

  - name: Stop and disable systemd-resolved
    ansible.builtin.service:
      name: systemd-resolved
      state: stopped
      enabled: no

  - name: Configure the resolv.conf with the dns server
    copy:
      dest: "/etc/resolv.conf"
      content: "nameserver {{ dns_server }}"

  - name: Disable systemd resolved
    ansible.builtin.service:
      name: systemd-resolved
      state: stopped
      enabled: no

  - community.docker.docker_compose:
      project_name: freeipa
      definition:
        version: '3.7'
        services:
          server:
            dns:
              - "{{ ipa_ip_address }}"
              - "{{ dns_server }}"
            image: freeipa/freeipa-server:centos-8-stream
            restart: unless-stopped
            hostname: "{{ ipa_domain }}"
            environment:
              IPA_SERVER_HOSTNAME: "{{ ipa_domain }}"
              TZ: "Europe/Paris"
            tty: true
            stdin_open: true
            cap_add:
              - NET_ADMIN
            volumes:
              - type: bind
                source: /etc/resolv.conf
                target: /etc/resolv.conf
              - /etc/localtime:/etc/localtime:ro
              - /sys/fs/cgroup:/sys/fs/cgroup:ro
              - /var/lib/ipa-data:/data
            sysctls:
              - net.ipv6.conf.all.disable_ipv6=0
              - net.ipv6.conf.lo.disable_ipv6=0
            command:
              - "-U"
              - "--domain={{ ipa_domain }}"
              - "--realm={{ ipa_realm }}"
              - "--ds-password={{ ipa_ds_password }}"
              - "--admin-password={{ ipa_admin_password }}"
              - "--setup-dns"
              - "--forwarder={{ dns_server }}"
              - "--no-host-dns"
              - "--unattended"
            ports:
              - "53:53"
              - "53:53/udp"
              - "80:80"
              - "443:443"
              - "389:389"
              - "636:636"
              - "88:88"
              - "464:464"
              - "88:88/udp"
              - "464:464/udp"
              - "123:123/udp"
              - "7389:7389"
              - "9443:9443"
              - "9444:9444"
              - "9445:9445"

  - name: Wait for the free-ipa start
    wait_for:
      path: /var/lib/ipa-data/var/log/ipaserver-install.log
      search_regex: "The ipa-server-install command was successful"
      timeout: 1200

  - name: Install the DHCP Server
    apt:
      name:
        - isc-dhcp-server
      state: present
      update_cache: yes

  - name: Configure the DHCP Server
    template:
      src: templates/dhcp.conf.j2
      dest: /etc/dhcp/dhcpd.conf

  - name: Configure the interfaces on which the DHCP Server listens
    copy:
      dest: "/etc/default/isc-dhcp-server"
      content: |
        INTERFACESv4="ens4"
        INTERFACESv6=""

  - name: Enable and start dhcpd
    ansible.builtin.service:
      name: isc-dhcp-server
      state: started
      enabled: true

  - name: Install the kerberos related stuff
    apt:
      name:
        - krb5-user
        - libpam-krb5
        - freeipa-client
      state: latest
      update_cache: yes

  - name: Install the NFS related stuff
    apt:
      name:
        - nfs-kernel-server
        - autofs
      state: latest
      update_cache: yes

  - name: Set up directory
    file:
      path: /srv/nfs4/homes/
      state: directory

  - name: Configure the DHCP Server
    template:
      src: templates/exports.j2
      dest: /etc/exports

  - name: Mount and bind a volume
    ansible.posix.mount:
      path: /srv/nfs4/homes
      src: /home
      opts: bind
      state: unmounted
      fstype: none

  - name: Mount and bind a volume
    ansible.posix.mount:
      path: /srv/nfs4/homes
      src: /home
      opts: bind
      state: mounted
      fstype: none

  - name: Add some IP address for the domain
    lineinfile:
      dest: /etc/hosts
      line: "{{ ipa_ip_address }} {{ ipa_domain }}"
      state: present

  - name: Configure as a free ipa client
    expect:
      command: ipa-client-install --force-join --mkhomedir
      responses:
        '(.*)Provide the domain name of your IPA server(.*)': "{{ ipa_domain }}"
        '(.*)Provide your IPA server name(.*)': "{{ ipa_domain }}"
        '(.*)Proceed with fixed values and no DNS discovery?(.*)': 'yes'
        '(.*)Do you want to configure chrony with NTP server or pool address?(.*)': 'no'
        '(.*)Continue to configure the system with these values(.*)': 'yes'
        '(.*)User authorized to enroll computers(.*)': "{{ ipa_admin_username }}"
        '(.*)Password for(.*)': "{{ ipa_admin_password }}"
      timeout: 36000
      creates: /etc/ipa/default.conf

  - name: Start service sssd, if not started
    ansible.builtin.service:
      name: sssd
      state: started
      enabled: yes

  - name: Configure the resolv.conf with freeipa as the dns server
    copy:
      dest: "/etc/resolv.conf"
      content: "nameserver 127.0.0.1"

  - name: Destroy existing tickets
    command: "kdestroy"

  - name: Log-In to kerberos
    expect:
      command: "kinit {{ ipa_admin_username }}"
      responses:
        "(.*)Password for (.*)": "{{ ipa_admin_password }}"

  - name: Check if the dns zone for the clients already exists
    command: "ipa dnszone-find {{ dhcp_clients_zone }}"
    register: dhcp_clients_zone_exists
    ignore_errors: True

  - name: Add a dns zone for the clients
    command: "ipa dnszone-add {{ dhcp_clients_zone }}"
    when: dhcp_clients_zone_exists.rc != 0

  - name: Check if the dns zone for the reverse exists
    command: "ipa dnszone-find in-addr.arpa."
    register: in_addr_arpa_zone_exists
    ignore_errors: True

  - name: Add the in-addr.arpa zone (reverse)
    command: "ipa dnszone-add in-addr.arpa. --skip-overlap-check"
    when: in_addr_arpa_zone_exists.rc != 0

  - name: Check if the A record for the FreeIPA server exists
    command: "ipa dnsrecord-find {{ ipa_domain }} --a-rec={{ ipa_ip_address }}"
    register: a_record_exists
    ignore_errors: True

  - name: Add A records for server
    command: "ipa dnsrecord-add {{ ipa_domain }} @ --a-ip-addr={{ ipa_ip_address }} --a-create-reverse"
    when: a_record_exists.rc != 0

  - name: Check if a service for the NFS already exists
    command: "ipa service-find nfs/{{ ipa_domain }}"
    register: nfs_service_exists
    ignore_errors: True

  - name: Create a service for the NFS
    command: "ipa service-add nfs/{{ ipa_domain }}"
    retries: "{{ retries }}"
    delay: "{{ delay }}"
    register: result
    until: result.rc == 0
    when: nfs_service_exists.rc != 0

  - name: Add the NFS principal to this machine
    command: "ipa-getkeytab -s {{ ipa_domain }} -p nfs/{{ ipa_domain }} -k /etc/krb5.keytab"

  - name: Check if A records for the clients exist
    command: "ipa dnsrecord-find {{ dhcp_clients_zone }} {{ item }} --a-rec={{ item }}"
    loop: "{{ dhcp_client_range_cidr | ip_in_range }}"
    register: clients_fqdn_exists
    ignore_errors: True

  - name: Add A records for the clients fqdn
    command: "ipa dnsrecord-add {{ dhcp_clients_zone }} {{ item.item }} --a-ip-addr={{ item.item }} --a-create-reverse"
    when: item.rc != 0
    loop: "{{ clients_fqdn_exists.results }}"

  - name: Start the nfs service, if not started
    ansible.builtin.service:
      name: nfs-server
      state: started
      enabled: yes

  - name: Configure automount
    command: "ipa-client-automount --unattended"
    register: automount_cmd
    failed_when: automount_cmd.rc != 0 and "An automount location is already configured" not in automount_cmd.stdout

  - name: Start the nfs service, if not started
    ansible.builtin.service:
      name: nfs-server
      state: restarted
      enabled: yes